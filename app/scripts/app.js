'use strict';

var itApp = angular.module('itApp', [
  'ngSanitize',
  'ngMessages',

  'ui.router',
  'ui.bootstrap',
  'ui.bootstrap.tpls',

  'itApp.widgets',
  /*'itApp.article',*/
  'itApp.example',
  'itApp.healthkit',
  'highcharts-ng',
  'angularMoment'
]);

itApp.directive('customInput', function() {
  return {
    scope: {
      numberModel: '=',
      name: '@',
      ngMinlength: '&'
    },
    template: '<input ng-minlength="ngMinlength()" name={{name}} ng-model="numberModel"/>'
  };
});

itApp.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider.state('default', {
    url: '/healthkit'
  });

  $stateProvider.state('about', {
    url: '/about',
    templateUrl: '/scripts/core/about.html',
    controller: 'AboutController',
    controllerAs: 'vm'
  });

  $urlRouterProvider.when('', '/healthkit');
});
