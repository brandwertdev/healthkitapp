'use strict';

angular.module('itApp.healthkit').controller('HealthkitDashboardController', function ($scope, healthkitService) {

  /* vm scope for all viewModel variables */
  var vm = this;
  /* global scope for all global used variables */
  var global = {};

  global.rawChartData = {
    healthkitData: [],
    repillData: []
  };

  global.defaultFilterParams = {
    startDate: getDefaultStartDate(),
    endDate: getDefaultEndDate(),
    identifiers: [
      {
        id: '66ADC441-161C-467E-8FF0-CEEF545ABAC2',
        name: 'Patrick Erni'
      },
      {
        id: 'AA9D07FD-FE98-487E-915C-6202A5CF2A75',
        name: 'Puneet Lal'
      },
      {
        id: '60BDAC18-E1E6-4690-9CD6-640CE650DD41',
        name: 'Hansruedi Jäggin'
      }
    ],
    medication: ["Aspirin 500", "Vitasprint B12", "Hiberix"]
  };

  global.healthkitFilterParams = {
    identifier: '',
    type: [],
    startdatefrom: '',
    startdateto: '',
    pagingstart: '',
    paginglength: ''
  };

  global.repillFilterParams = {
    identifier: '',
    medication: [],
    repillaction: '',
    scheduledatefrom: '',
    scheduledateto: '',
    takendatefrom: '',
    takendateto: '',
    pagingstart: '',
    paginglength: ''
  };

  /* init viewModel variables */
  vm.identifiers = global.defaultFilterParams.identifiers;
  vm.identifier = vm.identifiers[0];
  vm.startDateFormatted = moment(global.defaultFilterParams.startDate * 1000).toDate();
  vm.endDateFormatted = moment(global.defaultFilterParams.endDate * 1000).toDate();
  vm.dateFormats = ["todo"];


  $scope.callGetHealthkitData = function () {
    vm.startDate = vm.startDateFormatted.getTime() / 1000;
    vm.endDate = vm.endDateFormatted.getTime() / 1000;

    setHealthkitFilterParams(vm.startDate, vm.endDate, vm.identifier.id);
    getHealthkitData(global.healthkitFilterParams);
  };

  function getDefaultStartDate(){
    var defaultStartDate = new Date();
    defaultStartDate.setDate(defaultStartDate.getDate() - 7);
    defaultStartDate = Math.floor(defaultStartDate.getTime() / 1000);

    return defaultStartDate;
  }

  function getDefaultEndDate(){
    var defaultEndDate = new Date();
    defaultEndDate.setMinutes(defaultEndDate.getMinutes() + 10);
    defaultEndDate = Math.floor(defaultEndDate.getTime() / 1000);

    return defaultEndDate;
  }

  function setRepillFilterParams(startDate, endDate, identifier, medication){
    global.repillFilterParams = {
      identifier: identifier,
      medication: medication,
      repillaction: '',
      scheduledatefrom: '',
      scheduledateto: '',
      takendatefrom: startDate,
      takendateto: endDate,
      pagingstart: '',
      paginglength: ''
    };
  }

  function setHealthkitFilterParams(startDate, endDate, identifier){
    global.healthkitFilterParams = {
      identifier: identifier,
      type: [],
      startdatefrom: startDate,
      startdateto: endDate,
      pagingstart: '',
      paginglength: ''
    };
  }

  function getHealthkitData(healthkitFilterParams) {
    global.rawChartData.healthkitData = [];

    healthkitService.getHealthkitData(healthkitFilterParams).then(function (result) {
      global.rawChartData.healthkitData = result;

      vm.startDate = vm.startDateFormatted.getTime() / 1000;
      vm.endDate = vm.endDateFormatted.getTime() / 1000;

      setRepillFilterParams(vm.startDate, vm.endDate, vm.identifier.id, global.defaultFilterParams.medication);
      getRepillData(global.repillFilterParams);
    });
  }

  function getRepillData(repillFilterParams) {
    global.rawChartData.repillData = [];

    healthkitService.getRepillData(repillFilterParams).then(function (result) {
      global.rawChartData.repillData = result;

      buildChartData(global.rawChartData.healthkitData, global.rawChartData.repillData);
    });
  }

  function buildChartData(healthkitData, repillData) {
    var chartData = [];

    /* Define Y Axis multiplier to draw the Graphs */
    var bloodpressureDiastolicYFactor = 0.5;
    var bloodpressureSystolicYFactor = 0.5;
    var heartrateYFactor = 0.5;
    var oxygenSaturationYFactor = 60;
    var bodyMassYFactor = 0.0005;
    var HiberixYFactor = 1;
    var Aspirin500YFactor = 0.98;
    var VitasprintB12YFactor = 0.96;

    var takenColor = "#5cb85c";
    var skippedColor = "#C82E29";

    var repillHiberixGraph = {
      name: 'Repill Hiberix',
      marker: {enabled: true},
      data: [],
      lineWidth: 2
    };
    var repillVitasprintB12Graph = {
      name: 'Repill Vitasprint B12',
      marker: {enabled: true},
      data: [],
      lineWidth: 2
    };
    var repillAspirin500Graph = {
      name: 'Repill Aspirin 500',
      marker: {enabled: true},
      data: [],
      lineWidth: 2
    };
    var bloodpressureDiastolicGraph = {
      name: 'Bloodpressure Diastolic',
      marker: {enabled: true},
      data: []
    };
    var bloodpressureSystolicGraph = {
      name: 'Bloodpressure Systolic',
      marker: {enabled: true},
      data: []
    };
    var oxygenSaturationGraph = {
      name: 'Oxygen Saturation',
      marker: {enabled: true, fillColor: '#FFB848'},
      data: [],
      lineColor: '#FFB848',
      markerColor: '#FFB848'
    };
    var heartrateGraph = {
      name: 'Heartrate',
      marker: {enabled: true, fillColor: '#804C75'},
      data: [],
      lineColor: '#804C75'
    };
    var bodyMassGraph = {
      name: 'Body Weight',
      marker: {enabled: true, fillColor: '#CC00FF'},
      data: [],
      lineColor: '#CC00FF'
    };

    /* Fill HealthkitData into dedicated Array */
    for (var i = 0; i < healthkitData.length; i++) {

      var dataSet = healthkitData[i];

      if (dataSet['type'] == 'BloodPressureDiastolic') {

        var measurePoint = {
          name: 'Value: ' + dataSet['quantity'] + ' ' + dataSet['unit'] + '<br/>' + dataSet['startdate_format'],
          x: dataSet['startdate'] * 1000,
          y: dataSet['quantity'] * bloodpressureDiastolicYFactor
        };
        bloodpressureDiastolicGraph.data.push(measurePoint);
      }
      else if (dataSet['type'] == 'BloodPressureSystolic') {

        var measurePoint = {
          name: 'Value: ' + dataSet['quantity'] + ' ' + dataSet['unit'] + '<br/>' + dataSet['startdate_format'],
          x: dataSet['startdate'] * 1000,
          y: dataSet['quantity'] * bloodpressureSystolicYFactor
        };
        bloodpressureSystolicGraph.data.push(measurePoint);
      }
      else if (dataSet['type'] == 'HeartRate') {

        var measurePoint = {
          name: 'Value: ' + dataSet['quantity'] + ' ' + dataSet['unit'] + '<br/>' + dataSet['startdate_format'],
          x: dataSet['startdate'] * 1000,
          y: dataSet['quantity'] * heartrateYFactor
        };
        heartrateGraph.data.push(measurePoint);
      }
      else if (dataSet['type'] == 'OxygenSaturation') {

        var measurePoint = {
          name: 'Value: ' + dataSet['quantity'] + ' ' + dataSet['unit'] + '<br/>' + dataSet['startdate_format'],
          x: dataSet['startdate'] * 1000,
          y: (dataSet['quantity'] * 100 - 85) * 4.5
        };
        oxygenSaturationGraph.data.push(measurePoint);
      }
      else if (dataSet['type'] == 'BodyMass') {

        var tempWeight = dataSet['quantity'] / 1000;
        tempWeight = tempWeight.toFixed(2);
        var measurePoint = {
          name: 'Value: ' + tempWeight + ' kg <br/>' + dataSet['startdate_format'],
          x: dataSet['startdate'] * 1000,
          y: (dataSet['quantity']) * bodyMassYFactor
        };
        bodyMassGraph.data.push(measurePoint);
      }
    }
    chartData.push(bloodpressureDiastolicGraph);
    chartData.push(bloodpressureSystolicGraph);
    chartData.push(heartrateGraph);
    chartData.push(oxygenSaturationGraph);
    chartData.push(bodyMassGraph);

    /* Fill RepillData into dedicated Array */
    for (var i = 0; i < repillData.length; i++) {

      var dataSet = repillData[i];

      var pColor = takenColor;
      var takenDateFormat = dataSet['taken_date_format'];

      if (dataSet['repill_action'] == 'Skipped') {
        pColor = skippedColor;
        takenDateFormat = 'Skipped';
      }

      if (dataSet['medication'] == 'Hiberix') {

        var measurePoint = {
          name: 'Quantity: ' + dataSet['quantity'] + ' ' + dataSet['unit'] + '<br/>S: ' + dataSet['schedule_date_format'] + '<br/><span style="font-weight: bold">T: ' + takenDateFormat +'</span>',
          x: dataSet['taken_date'] * 1000,
          y: 100 * HiberixYFactor,
          color: pColor
        };
        repillHiberixGraph.data.push(measurePoint);
      }
      else if (dataSet['medication'] == 'Vitasprint B12') {

        var measurePoint = {
          name: 'Quantity: ' + dataSet['quantity'] + ' ' + dataSet['unit'] + '<br/>S: ' + dataSet['schedule_date_format'] + '<br/><span style="font-weight: bold">T: ' + takenDateFormat +'</span>',
          x: dataSet['taken_date'] * 1000,
          y: 100 * VitasprintB12YFactor,
          color: pColor
        };
        repillVitasprintB12Graph.data.push(measurePoint);
      }
      else if (dataSet['medication'] == 'Aspirin 500') {

        var measurePoint = {
          name: 'Quantity: ' + dataSet['quantity'] + ' ' + dataSet['unit'] + '<br/>S: ' + dataSet['schedule_date_format'] + '<br/><span style="font-weight: bold">T: ' + takenDateFormat +'</span>',
          x: dataSet['taken_date'] * 1000,
          y: 100 * Aspirin500YFactor,
          color: pColor
        };
        repillAspirin500Graph.data.push(measurePoint);
      }
    }
    chartData.push(repillHiberixGraph);
    chartData.push(repillAspirin500Graph);
    chartData.push(repillVitasprintB12Graph);

    drawChart(chartData);
  }

  function drawChart(chartData) {

    vm.highchartsNG = {
      title: {
        text: 'REPILL HEALTH CHART',
        style: {
          color: '#00659F',
          textTransform: 'uppercase',
          fontSize: '18px'
        }
      },
      subtitle: {
        text: 'This Chart is showing vital data from Apple Health App and medication intake data from Absolute Development RePill App',
        style: {
          color: '#00659F',
          textTransform: 'uppercase',
          fontSize: '11px'
        }
      },
      xAxis: {
        gridLineColor: '#e0e0e0',
        labels: {
          style: {
            color: '#00659F'
          }
        },
        lineColor: '#00659F',
        minorGridLineColor: '#00659F',
        tickColor: '#00659F',
        tickWidth: 3,
        title: {
          style: {
            color: '#c7e5f3'
          }
          /*text: 'Date'*/
        },
        type: 'datetime',
        dateTimeLabelFormats: { // don't display the dummy year
          month: '%e. %b',
          year: '%b'
        }
      },
      yAxis: {
        showAxis: false,
        gridLineColor: '#c9c9c9',
        labels: {
          enabled: false,
          style: {
            color: '#154661'
          }
        },
        lineColor: '#c9c9c9',
        minorGridLineColor: '#000000',
        tickColor: '#c9c9c9',
        tickWidth: 0,
        title: {
          style: {
            color: '#c7e5f3'
          }
        }
      },
      plotOptions: {
        spline: {
          marker: {
            enabled: true
          }
        }
      },
      options: {
        chart: {
          type: 'spline',
          colors: ["#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
            "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
          backgroundColor: {
            linearGradient: {x1: 0, y1: 0, x2: 1, y2: 1},
            stops: [
              [0, '#ffffff'],
              [1, '#ffffff']
            ]
          },
          style: {
            fontFamily: "Helvetica, Arial, sans-serif"
          },
          plotBorderColor: '#606063'
        },
        legend: {
          itemStyle: {
            color: '#00659F'
          },
          itemHoverStyle: {
            color: '#6AC8EE'
          },
          itemHiddenStyle: {
            color: '#55BCFF'
          },
          style: {
            fontStyle: 'normal'
          }
        },
        tooltip: {
          enabled: true,
          headerFormat: '<b>{series.name}</b><br>',
          pointFormat: '{point.name}'
        }
      },
      series: chartData
    }
  }

  // Load data on first page call
  setRepillFilterParams(global.defaultFilterParams.startDate, global.defaultFilterParams.endDate, global.defaultFilterParams.identifiers[0].id, global.defaultFilterParams.medication);
  setHealthkitFilterParams(global.defaultFilterParams.startDate, global.defaultFilterParams.endDate, global.defaultFilterParams.identifiers[0].id);

  getHealthkitData(global.healthkitFilterParams);
});

/* Directive to format the DateTime Field */
angular.module('itApp.healthkit').directive('moDateInput', function ($window) {
  return {
    require: '^ngModel',
    restrict: 'A',
    link: function (scope, elm, attrs, ctrl) {
      var moment = $window.moment;
      var dateFormat = attrs.moMediumDate;

      attrs.$observe('moDateInput', function (newValue) {
        if (dateFormat == newValue || !ctrl.$modelValue) return;
        dateFormat = newValue;
        ctrl.$modelValue = new Date(ctrl.$setViewValue);
      });

      ctrl.$formatters.unshift(function (modelValue) {
        scope = scope;
        if (!dateFormat || !modelValue) return "";
        var retVal = moment(modelValue).format(dateFormat);
        return retVal;
      });

      ctrl.$parsers.unshift(function (viewValue) {
        scope = scope;
        var date = moment(viewValue, dateFormat);
        return (date && date.isValid() && date.year() > 1950 ) ? date.toDate() : "";
      });
    }
  };
});




