'use strict';

angular.module('itApp.healthkit').factory('healthkitService', healthkitService);

healthkitService.$inject = ['$q', '$http'];

function healthkitService($q, $http) {
  var cachedData = null;

  return {
    getHealthkitData: getHealthkitData,
    getRepillData: getRepillData
  };

  function getHealthkitData(healthkitDataParams) {

    var queryString = buildQueryString(healthkitDataParams);


    /* if(cachedData) {
     return $q.when(cachedData);
     }
     else {*/
    return $http.get('http://83.169.13.252:8081/api/healthkit/' + queryString).then(function (response) {
      // store the data in the cache variable
      /*cachedData = response.data;*/

      return response.data;
    });
    /*}*/
  }

  function getRepillData(repillDataParams) {

    var queryString = buildQueryString(repillDataParams);


    /*if(cachedData) {
     return $q.when(cachedData);
     }
     else {*/
    return $http.get('http://83.169.13.252:8081/api/repill/' + queryString).then(function (response) {

      return response.data;
    });
    /*}*/
  }

  function buildQueryString(dataParams) {
    var queryString = '';

    for (var paramName in dataParams) {

      if (dataParams[paramName] instanceof Array) {
        for (var i = 0; i < dataParams[paramName].length; i++) {
          queryString = queryString.concat('&', paramName, '=', dataParams[paramName][i]);
        }
      }
      else if ((typeof dataParams[paramName] === 'string' && dataParams[paramName].length) || typeof dataParams[paramName] === 'number') {
        queryString = queryString.concat('&', paramName, '=', dataParams[paramName]);
      }
    }
    queryString = queryString.replace('&', '?');

    return queryString;
  }

}
