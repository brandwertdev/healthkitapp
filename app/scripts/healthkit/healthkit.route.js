'use strict';

angular.module('itApp.healthkit').config(function ($stateProvider) {
  $stateProvider.state('healthkitdashboard', {
    url: '/healthkit',
    templateUrl: 'views/healthkit/healthkit.dashboard.html',
    controller: 'HealthkitDashboardController',
    controllerAs: 'vm',
    inMenu: false,
    menuTitle: 'Dashboard'
  });

  /*$stateProvider.state('articledetail', {
    url: '/articles/:id',
    templateUrl: '/scripts/article/article.detail.html',
    controller: 'ArticleDetailController',
    controllerAs: 'vm',
    resolve: {
      article: function($stateParams, articleService) {
        var articleId = parseInt($stateParams.id, 10);
        return articleService.getArticleById(articleId);
      }
    }
  });*/
});
