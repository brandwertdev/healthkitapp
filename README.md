# Healthkit APP

Die APP liest über ein REST API Gesundheitsdaten und stellt diese zur Statistikzwecken graphisch dar. Die Dateneingabe erfolgt über die von uns entwickelte Repill APP: http://www.repillapp.com/de/


## Funktionsauszug
* Datencollection REST API
* Graphische Statistik
* Filterung verschiedener Werte


## Technologien
* AngularJS 1.5
* REST API
* Grunt
* Bower